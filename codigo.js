(function(){

  var horaActual = function(){
      var fecha = new Date(),
          hora = fecha.getHours(),
          ampm,
          minutos = fecha.getMinutes(),
          segundos = fecha.getSeconds(),
          diaSemana = fecha.getDay();

      var pHora = document.getElementById('hora'),
          pAMPM = document.getElementById('ampm'),
          pMinutos = document.getElementById('minutos'),
          pSegundos = document.getElementById('segundos'),
          pDiaSemana = document.getElementById('diaSemana');

      var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
      pDiaSemana.textContent = semana[diaSemana];

      if (hora >= 12) {
        hora = hora -12;
        ampm = 'PM';
      }else{
        ampm = 'AM';
      }

      if (hora == 0) {
        hora = 12;
      }

      pHora.textContent = hora;
      pAMPM.textContent = ampm;

      if (minutos < 10) {minutos = "0" + minutos};
      if (segundos < 10) {segundos = "0" + segundos};
      pMinutos.textContent = minutos;
      pSegundos.textContent = segundos;
  };
  horaActual();
}())

console.log('document.location', document.location.href);
